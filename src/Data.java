/**
 * You shall not work with non-native classes / libraries; You shall not make
 * use of neither Data nor Calendar classes; If the op is not valid an exception
 * must be thrown; If the value is smaller than zero, you should ignore its
 * signal; If the result sum is bigger than max value to the field, you should
 * increment its immediate bigger field; Ignore the fact that February have
 * 28/29 days and always consider only 28 days; Ignore the daylight save time
 * rules.
 * 
 * @author Samuel Catalano
 * 
 */
public class Data {

	private Integer dia;
	private Integer mes;
	private Integer ano;
	private Integer hora;
	private Integer minuto;
	private Integer minutosDoDia;

	/**
	 * Change the date add minutes
	 * 
	 * @param date
	 * @param op
	 * @param value
	 * @return the date
	 * @throws Exception
	 */
	public String changeDate(String date, char op, long value) throws Exception {
		if (value < 0) {
			value = value * -1;
			op = '-';
		}

		if ((op != '-') && (op != '+')) {
			throw new IllegalArgumentException("This op is invalid.");
		}

		desmembrarDataString(date);
		adicionaDiasNaData(countDays(value, op), op);

		return print();
	}

	private Integer countDays(long minutes, char op) {
		Integer days = 0;

		while (minutes != 0) {
			if (op == '+') {
				if (this.minutosDoDia.equals(1439)) {
					this.minutosDoDia = 0;
					days++;
				} else {
					this.minutosDoDia++;
				}
			}

			if (op == '-') {
				if (this.minutosDoDia.equals(0)) {
					this.minutosDoDia = 1439;
					days++;
				} else {
					this.minutosDoDia--;
				}
			}

			minutes--;
		}
		return days;
	}

	/**
	 * Adiciona dias na data
	 * 
	 * @param days
	 * @param op
	 */
	private void adicionaDiasNaData(Integer days, char op) {
		Meses meses;

		while (days != 0) {
			if (op == '+') {
				meses = Meses.getMonth(this.mes);

				if (this.dia.intValue() == meses.getQuantidadeDiasNoMes()) {
					this.dia = 1;

					if (this.mes.intValue() == 12) {
						this.ano++;
						this.mes = 1;
					} else {
						this.mes++;
					}
				} else {
					this.dia++;
				}
			}

			if (op == '-') {
				if (this.dia == 1) {
					if (this.mes == 1) {
						this.ano--;
						this.mes = 12;
					} else {
						this.mes--;
						meses = Meses.getMonth(this.mes);
						this.dia = meses.getQuantidadeDiasNoMes();
					}
				} else {
					this.dia--;
				}
			}

			days--;
		}
	}

	/**
	 * Desmembra a data que vem em String
	 * 
	 * @param dateHour
	 */
	private void desmembrarDataString(String dateHour) {
		String[] dates = dateHour.split(" ");

		if (dates != null) {
			if (dates.length == 2) {
				String[] date = dates[0].split("/");
				this.dia = Integer.valueOf(date[0]);
				this.mes = Integer.valueOf(date[1]);
				this.ano = Integer.valueOf(date[2]);

				String[] hour = dates[1].split(":");
				this.hora = Integer.valueOf(hour[0]);
				this.minuto = Integer.valueOf(hour[1]);
				this.minutosDoDia = (this.hora * 60) + this.minuto;
			}
		}
	}

	/**
	 * @param minutes
	 */
	private void converteMinutosDoDiaParaHorasEMinutos(long minutes) {
		Long minute = minutes % 60;
		Long hour = (minutes - minute) / 60;

		this.hora = Integer.valueOf(hour.toString());
		this.minuto = Integer.valueOf(minute.toString());
	}

	/**
	 * Imprimir.
	 * 
	 * @return
	 */
	private String print() {
		converteMinutosDoDiaParaHorasEMinutos(this.minutosDoDia);

		String d = this.dia.toString();
		String m = this.mes.toString();
		String h = this.hora.toString();
		String mi = this.minuto.toString();

		if (this.dia < 10) {
			d = "0" + this.dia;
		}

		if (this.mes < 10) {
			m = "0" + this.mes;
		}

		if (this.hora < 10) {
			h = "0" + this.hora;
		}

		if (this.minuto < 10) {
			mi = "0" + this.minuto;
		}

		return d + "/" + m + "/" + this.ano + " " + h + ":" + mi;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Data date = new Data();

		try {
			System.out.println(date.changeDate("01/03/2010 23:00", '+', 4000));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the dia
	 */
	public Integer getDia() {
		return dia;
	}

	/**
	 * @param dia
	 *            the dia to set
	 */
	public void setDia(Integer dia) {
		this.dia = dia;
	}

	/**
	 * @return the mes
	 */
	public Integer getMes() {
		return mes;
	}

	/**
	 * @param mes
	 *            the mes to set
	 */
	public void setMes(Integer mes) {
		this.mes = mes;
	}

	/**
	 * @return the ano
	 */
	public Integer getAno() {
		return ano;
	}

	/**
	 * @param ano
	 *            the ano to set
	 */
	public void setAno(Integer ano) {
		this.ano = ano;
	}

	/**
	 * @return the hora
	 */
	public Integer getHora() {
		return hora;
	}

	/**
	 * @param hora
	 *            the hora to set
	 */
	public void setHora(Integer hora) {
		this.hora = hora;
	}

	/**
	 * @return the minuto
	 */
	public Integer getMinuto() {
		return minuto;
	}

	/**
	 * @param minuto
	 *            the minuto to set
	 */
	public void setMinuto(Integer minuto) {
		this.minuto = minuto;
	}

	/**
	 * @return the minutosDoDia
	 */
	public Integer getMinutosDoDia() {
		return minutosDoDia;
	}

	/**
	 * @param minutosDoDia
	 *            the minutosDoDia to set
	 */
	public void setMinutosDoDia(Integer minutosDoDia) {
		this.minutosDoDia = minutosDoDia;
	}
}