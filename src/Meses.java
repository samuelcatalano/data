public enum Meses {

	JANEIRO(1, 31), 
	FEVEREIRO(2, 28), 
	MARCO(3, 31), 
	ABRIL(4, 30), 
	MAIO(5, 31), 
	JUNHO(6, 30), 
	JULHO(7, 31), 
	AGOSTO(8, 31), 
	SETEMBRO(9, 30), 
	OUTUBRO(10, 31), 
	NOVEMBRO(11, 30), 
	DEZEMBRO(12, 31);

	private final int numeroDoMes;
	private final int quantidadeDiasNoMes;

	/**
	 * Construtor
	 * @param num
	 * @param numDay
	 */
	Meses(int num, int numDay) {
		this.numeroDoMes = num;
		this.quantidadeDiasNoMes = numDay;
	}

	public int getNumeroDoMes() {
		return numeroDoMes;
	}

	public int getQuantidadeDiasNoMes() {
		return quantidadeDiasNoMes;
	}

	public static Meses getMonth(int num) {
		Meses[] values = Meses.values();

		for (Meses item : values) {
			if (item.getNumeroDoMes() == num) {
				return item;
			}
		}
		return null;
	}
}